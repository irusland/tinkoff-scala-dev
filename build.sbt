ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .settings(
    name := "homework-1"
  )

resolvers +=
  "scala-course" at "https://gitlab.com/api/v4/projects/33751126/packages/maven"

libraryDependencies += "ru.tinkoff" % "scala-course-tom_2.13" % "0.2"
libraryDependencies += "ru.tinkoff" % "scala-course-john_2.13" % "0.2"

dependencyOverrides += "ru.tinkoff" % "scala-course-clerk_2.13" % "0.1"  force()
